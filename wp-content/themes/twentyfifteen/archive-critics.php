<?php get_header(); 

?>
<?php if ($post->post_type=='articles'):?>
<div class="main">
    <div class="container">
        <section class="section">
            <div class="row" id="box_container">
                <?php print_r($post);  $args = array(    'posts_per_page' => -1,  // Get all posts 
                                           'post_type' => 'articles',  // The Custom Post Type we named 'products'   
                                                );  
                                $articles = get_posts( $args );  


                                foreach ( $articles as $article ) : ?>    
                                
                                

                                <?php

                                  $fields = get_fields($article);
                                  $title_main = $article->post_title; 
                                  $content_main = $article->post_content;  
                                  $first_image = $fields['first_image'];
                                  //print_r($first_image)
                                  $time = get_the_date();
                                  //print_r($fields);


                                  echo '


                                  <div class="col-md-4 col-sm-12">
                                            <a href="' . get_permalink($article)  . '" class="box little_box">
                                                <div class="img_holder">
                                                    <img src="'.$first_image.'" />
                                                </div> <!--img_holder-->
                                                <div class="brief_info">
                                                    <h2>'. $title_main .'</h2>
    
                                                    <time>'.$time.'</time>
                                                </div> <!--brief_info-->
                                            </a>
                                        </div>
                                    

                                  ';

                                  ?>


             <?php endforeach;?>
             </div> <!--row-->
            <div class="more">
                <a href="#" class="btn more_btn">
                    بیشتر
                </a>
            </div> <!--more-->
        </section> <!--section-->
    </div> <!--container-->
</div> <!--main-->
<?php endif ?>

<?php get_footer(); ?>