<?php get_header(); 

?>
<?php if ($post->post_type=='artists'):?>
<div class="main">
    <div class="container">
        <section class="section">
            <div class="row" id="box_container">
                <?php   $args = array(    'posts_per_page' => -1,  // Get all posts 
                                                  'post_type' => 'artists',  // The Custom Post Type we named 'products'   
                                                );  
                                $articles = get_posts( $args );  


                                foreach ( $articles as $article ) : ?>    
                                
                                

                                <?php

                                  $fields = get_fields($article);
                                  $title_main = $article->post_title; 
                                  $content_main = $article->post_content;  
                                  $first_image = $fields['first_image'];
                                  //print_r($first_image)
                                  $time = get_the_date();
                                  //print_r($fields);


                                  echo '


                                  <div class="col-md-4 col-sm-12">
                                            <a href="' . get_permalink($article)  . '" class="box little_box">
                                            <div class="image_container">
                                                <div class="img_holder">
                                                    <img src="'.$first_image.'" />
                                                </div> <!--img_holder-->
                                                </div>
                                                <div class="brief_info">
                                                    <h2>'. $title_main .'</h2>
    
                                                    <time>'.$time.'</time>
                                                </div> <!--brief_info-->
                                            </a>
                                        </div>
                                    

                                  ';

                                  ?>


             <?php endforeach;?>
             </div> <!--row-->
             <?php 
                  
                  if (sizeof($articles) > 9):?>
                    <div class="more">
                          <a data-type="artists" data-catid="9" class="btn more_btn">
                              بیشتر
                          </a>
                     </div> <!--more-->
              <?php endif; ?>
        </section> <!--section-->
    </div> <!--container-->
</div> <!--main-->
<?php endif ?>
<?php if ($post->post_type=='events'):?>
<div class="main">
    <div class="container">
        <section class="section">
            <div class="row" id="box_container">
                <?php   $args = array(    'posts_per_page' => -1,  // Get all posts 
                                                  'post_type' => 'events',  // The Custom Post Type we named 'products'   
                                                );  
                                $events = get_posts( $args );  


                                foreach ( $events as $event ) : ?>    
                                
                                

                                <?php

                                  $fields = get_fields($event);
                                  $title_main = $event->post_title; 
                                  $content_main = $event->post_content;  
                                  $first_image = $fields['first_image'];
                                  //print_r($first_image)
                                  $time = $fields['time'];
                                  //print_r($fields);


                                  echo '


                                  <div class="col-md-4 col-sm-12">
                                            <a href="' . get_permalink($event)  . '" class="box little_box">
                                            <div class="image_container">
                                                <div class="img_holder">
                                                    <img src="'.$first_image.'" />
                                                </div> <!--img_holder-->
                                                </div>
                                                <div class="brief_info">
                                                    <h2>'. $title_main .'</h2>
                                           
                                                    <time>'.$time.'</time>
                                                </div> <!--brief_info-->
                                            </a>
                                        </div>
                                    

                                  ';

                                  ?>


             <?php endforeach;?>
             </div> <!--row-->
            <?php 
                  
                  if (sizeof($events) > 9):?>
                    <div class="more">
                          <a data-type="events" data-catid="9" class="btn more_btn">
                              بیشتر
                          </a>
                     </div> <!--more-->
              <?php endif; ?>
        </section> <!--section-->
    </div> <!--container-->
</div> <!--main-->
<?php endif ?>
<?php get_footer(); ?>