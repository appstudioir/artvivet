var posts_offset  = 9;
var template = '{{#.}} <div class="col-md-4 col-sm-12">'+
                         '<a href="" class="box little_box">'+
                            '<div class="img_holder">'+
                                '<img src="" />'+
                            '</div>'+
                             '<div class="brief_info">'+
                                    '<h2>{{post_title}}</h2>'+
                                    '<time>{{f_date}}</time>'+
                             '</div>'+
                            '</a>'+
                        '</div>{{/.}}';

                        
                        
var artvivet = {
    load_more : function(){
        //alert(posts_offset);
        jQuery(".more_btn").find('i').addClass('loop');
        var cat_id = jQuery(".more_btn").data("catid");
        var post_type = jQuery(".more_btn").data("type");
        jQuery.get("/api/?category="+cat_id+"&offset="+posts_offset+"&type="+post_type)
        .success(function(data){
            var posts = data;

            var rendered = Mustache.render(template, posts);
            $('#box_container').append(rendered);
            posts_offset +=9;
            $(".img_holder").imgLiquid({
                fill: true,
                horizontalAlign: "center",
                verticalAlign: "center"
            });
            jQuery(".more_btn").find('i').removeClass('loop');
            if (posts.length < 9) {


                jQuery(".more_btn").hide();


            };

            js.init();


        })

         
    }
}