

	AccessifyHTML5();
	//var $j = jQuery.noConflict();
	var app = {};
	//////////////////////////////////////////////////////////////

	var js = {

    init: function(){

    	js.show_fixed_toolbar();
    	js.show_searchbar();
    	js.add_random_class();
    	js.fancy_box();
		js.inner_image();
		js.empty_gallery();
		js.language();
		js.hide_star();
		js.artist_info();
		js. empty_field();
	   	$(".img_holder").imgLiquid({
	        fill: true,
	        horizontalAlign: "center",
	        verticalAlign: "center"
	    });
	    js.validation();


    },

    fancy_box:function(){


    	$(".fancybox-thumb").fancybox({
	    	openEffect	: 'none',
	    	closeEffect	: 'none',
	    	loop:true,

	    	helpers : {
	    		thumbs	: {
					width	: 50,
					height	: 50
				},
				overlay: {
			      locked: false
			    }

	    	}
	    });



    },

    add_random_class:function(){

    	var random = Math.floor(Math.random() * 1000);
    	var randoms = Math.floor(Math.random() * 2500);

		var $box = $(".box");
		/*$box.eq(random % $box.length).addClass("medium_box");*/

		console.log($('.medium_box').length);

		/*$box.eq(randoms % ($box.length - $('.medium_box').length ) ).addClass("big_box");*/


			/*console.log($('.big_box').length);*/



		// var $container2 = $('#box_container');
		
		// 	var holder_width = $container2.width();
		// 	$container2.packery({
		// 	  itemSelector: '.col-md-4',
		// 	  // layoutMode: 'masonry',
		// 	  //  gutter: 0
		// 	})	

			 // $container2.isotope('shuffle');

    },
        
    show_fixed_toolbar:function(){
    	if ($('.toolbar_on_scroll').length > 0) {


    			$('header').waypoint(function(direction) {
		        	if (direction === 'down') {
			          $('.toolbar_on_scroll').slideDown(300)
				        }
				      }, {
				        offset: '-180'
			      });

			      $('header').waypoint(function(direction) {
			        if (direction === 'up') {
			          $('.toolbar_on_scroll').slideUp(200)
			          //alert()
			        }
			      }, {
			        offset: '-180'
			      });



    	};

   
    },

     
    
	inner_image:function(){
		var height = $('.inner .img_holder').width();
		$('.inner .img_holder').height(height);
	},
   	
	empty_gallery:function(){
		if($('.inner .gallery li').length< 1) {
			$('#gallery').css({'display':'none'});
		}
	},

	language:function(){
		$('.lang li').on('click', function(event){
			event.preventDefault();
			$('.lang li').removeClass('active');
			$(this).addClass('active');
		})
	},
	show_searchbar:function(){
	    $('.search_btn').on('click', function(event){
	    	event.preventDefault();
	    	var layoutHeight = $('body').height
	    	$('#search_layout').height(layoutHeight);
	    	$('.search_layout').stop().slideDown(500);
	    	$('#search_place').focus();
	    })
	    $('.close_big').on('click', function(event){
	    	event.preventDefault();
	    	$('.search_layout').stop().slideUp(500);

	    })

		
    },

    hide_star:function(){
    	$('.required input').keydown(function(){
    		$(this).parent().find('i').hide();
    	})
    	$('.required textarea').keydown(function(){
    		$(this).parent().find('i').hide();
    	})
    	$('.required input').focusout(function(){
  			if($(this).val().length > 0){
  				$(this).parent().find('i').hide();
  			}
  			else{
  				$(this).parent().find('i').show();
  			}
		});
		$('.required input').keyup(function(){
  			if($(this).val().length > 0){
  				$(this).parent().find('i').hide();
  			}
  			else{
  				$(this).parent().find('i').show();
  			}
		});
		$('.required textarea').focusout(function(){
  			if($(this).val().length > 0){
  				$(this).parent().find('i').hide();
  			}
  			else{
  				$(this).parent().find('i').show();
  			}
		});
		$('.required textarea').keyup(function(){
  			if($(this).val().length > 0){
  				$(this).parent().find('i').hide();
  			}
  			else{
  				$(this).parent().find('i').show();
  			}
		});

		
		$('.comment-form .comment-form-author').keydown(function(){
    		$(this).addClass('nonstar');
    	})
    	$('.comment-form .comment-form-email').keydown(function(){
    		$(this).addClass('nonstar');
    	})
    	$('.comment-form .comment-form-comment').keydown(function(){
    		$(this).addClass('nonstar');
    	})
    	$('.comment-form .comment-form-author').focusout(function(){
  			if($(this).find('input').val().length > 0){
  				$(this).addClass('nonstar');
  			}
  			else{
  				$(this).removeClass('nonstar');
  			}
		});
    	$('.comment-form .comment-form-email').focusout(function(){
  			if($(this).find('input').val().length > 0){
  				$(this).addClass('nonstar');
  			}
  			else{
  				$(this).removeClass('nonstar');
  			}
		});
		$('.comment-form .comment-form-comment').focusout(function(){
  			if($(this).find('textarea').val().length > 0){
  				$(this).addClass('nonstar');
  			}
  			else{
  				$(this).removeClass('nonstar');
  			}
		});
		$('.comment-form .comment-form-author').keyup(function(){
  			if($(this).find('input').val().length > 0){
  				$(this).addClass('nonstar');
  			}
  			else{
  				$(this).removeClass('nonstar');
  			}
		});
    	$('.comment-form .comment-form-email').keyup(function(){
  			if($(this).find('input').val().length > 0){
  				$(this).addClass('nonstar');
  			}
  			else{
  				$(this).removeClass('nonstar');
  			}
		});
		$('.comment-form .comment-form-comment').keyup(function(){
  			if($(this).find('textarea').val().length > 0){
  				$(this).addClass('nonstar');
  			}
  			else{
  				$(this).removeClass('nonstar');
  			}
		});
 
		



    },

    artist_info:function(){
    	if($('.artist_info h5').text() ==""){
    		$('.artist_info').find('span').hide();
    	}

    },



    empty_field:function(){
    	$('#btn_search').on('click' ,function(){
    		if(!$(this).parent().find('#search_place').val()){
    			 return false
    		}
    	})
    	$('#btn_search_footer').on('click' ,function(){
    		if(!$(this).parent().find('.search_place').val()){
    			 return false
    		}
    	})

    },

    validation:function(){

    	$(".filling_form").validate({

                rules: {
                    user_name: {
                        required: true,
                        minlength: 3,
                    },
                  
                    email: {
                      required: true,
                     
                    },
                    description: {
                      required: true,
                      minlength: 5,
                     
                    },
                    
                    },
                         messages: {
                 user_name: {
                    required:  "please enter your name" ,
                    minlength: "The number of characters is not valid" ,
                },
                
                email: {
                    email : "please enter the valid email",
                    required: "please enter your email",
                },
                description: {
                    
                    required: "Please enter your message",
                    minlength: "The number of characters is not valid",
                },
               

            } 

            });

    $("#newsletter").validate({
          rules: {
                  
                  
                    email: {
                      required: true,
                     
                    },
                  },

                     messages: {
                 
                
                      email: {
                          email : "please enter the valid email",
                          required: "please enter your email",
                      },
                    }
      });


    },

}











	//////////////////////////////////////////////////////////////
	$(document).ready(function() {

		js.init();

		$(".empty_val").val("");

		$(".more_btn").click(function(){
			artvivet.load_more();
		})
	});


	/**
	*
	* Custom Triggers
	*
	**/
	

	$( window ).on({

		'load': function( e ) {

		},

		'scroll': function( e ) {

		},

		'resize': function( e ) {

		},

		'mousemove': function( e ) {

		},

		'mouseup': function( e ) {

		},

		'keydown': function( e ) {

		},

		'keyup': function( e ) {

		},

		'mousewheel DOMMouseScroll': function( e ) {

		},

	});

