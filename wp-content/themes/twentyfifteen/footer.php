<?php 
    $page = get_page_by_title('home');
    $fields = get_fields($page);
?>
<footer>
            <div class="container">
                
                <div class="top_footer clearfix">
                    <a href="#">
                        <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo-f.png" />
                    </a>
                    <span></span>
                </div> <!--top_footer-->
                <div class="row clearfix">
                    <div class="col-md-8 col-sm-12">
                        <div class="clearfix">
                            <ul class="menu">
                                <li>
                                    <a href="/">
                                        صفحه اصلی
                                    </a>
                                </li>
                                <li class="drop">
                                    <a href="#">
                                        مقالات
                                    </a>
                                    <ul>
                                        <li>
                                            <a href="/article">
                                                مقالات
                                            </a>
                                        </li>
                                        <li>
                                            <a href="/critic">
                                                بررسی آثار
                                            </a>
                                        </li>
                                    </ul> 
                                </li> <!--drop-->
                                <li>
                                    <a href="/events">
                                        نمایشگاه ها
                                    </a>
                                </li>
                                <li>
                                    <a href="/artists">
                                        هنرمندان
                                    </a>
                                </li>
                                <li>
                                    <a href="/submit-works">
                                        ارسال اثر
                                    </a>
                                </li>
                                <li>
                                    <a href="/contact">
                                        تماس با ما
                                    </a>
                                </li>
                            </ul> <!--menu-->
                        </div> 
                        <div class="privacy">
                            <a href="#">
                                قوانین و مقررات
                            </a>
                            <a href="#">
                                privacy policy
                            </a>
                        </div> <!--privacy-->
                        <div class="bottom_footer">
                            <p>
                                Desigend by: <a href="http://app-studio.ir/">App Studio</a>
                            </p>
                            <p>
                                Copyright © 2015 ArtVivet. All rights reserved.
                            </p>
                        </div> <!--bottom_footer-->
                    </div>
                    <div class="col-md-4 col-sm-12 side_footer">
                        
                        <form id="newsletter">
                            <p>عضویت در خبرنامه</p>
                            <input type="email" placeholder="ایمیل" name="email" />
                            <button type="submit">
                                <i class="icon footer_send_icon"></i>
                            </button>
                           
                            
                        </form>
                        
                        <ul class="social">
                            <li>
                                <a href="<?php echo $fields['facebook'] ?>">
                                    <i class="icon fb_icon"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $fields['twitter'] ?>">
                                    <i class="icon tweet_icon"></i>
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo $fields['instagram'] ?>">
                                    <i class="icon insta_icon"></i>
                                </a>
                            </li>
                        </ul> <!--social-->
                    </div>
                </div> <!--row-->
            </div> <!--container-->
        </footer>






        <!--[if lt IE 9]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->






        <script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri() ?>/assets/js/vendor/jquery-1.11.0.min.js"><\/script>')</script>
        <script src="<?php echo get_template_directory_uri() ?>/assets/js/plugins.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/assets/js/main.js"></script>
        <script src="<?php echo get_template_directory_uri() ?>/assets/js/dev.js"></script>
   
        <script src="<?php echo get_template_directory_uri() ?>/assets/js/append_link.js"></script>
<script type="text/javascript">
    function addLink() {

    var body_element = document.getElementsByTagName('body')[0];
    var selection;
    selection = window.getSelection();
    var linebreaks = '';
    var link_name;

    var append_link = [];
    append_link.prepend_break =  <?php echo $GLOBALS['params']["prepend_break"]; ?>;
    append_link.use_title =  <?php echo $GLOBALS['params']["use_title"]; ?>;
    append_link.add_site_name =  <?php echo $GLOBALS['params']["add_site_name"]; ?>;
    append_link.always_link_site =  <?php echo $GLOBALS['params']["always_link_site"]; ?>;
    append_link.read_more =  '<?php echo $GLOBALS["params"]["read_more"]; ?>';


    for (i = 0; i < append_link.prepend_break; i++) {
        linebreaks = linebreaks + '<br />';
    }

    if (append_link.use_title == 'true') {
        link_name = append_link.page_title;
    }
    else {
        link_name = document.URL
    }

    if (append_link.add_site_name == 'true') {
        link_name += ' | ' + append_link.site_name;
    }

    if (append_link.always_link_site == true) {
        link_url = append_link.site_url;
    }
    else {
        link_url = document.URL;
    }

    var pagelink =
        linebreaks
        + ' ' + append_link.read_more + ' ';

    pagelink = pagelink.replace('%link%', ' ' + link_url + ' ');

    var copytext = selection + pagelink;
    var newdiv = document.createElement('div');

    newdiv.style.position='absolute';
    newdiv.style.left='-99999px';
    body_element.appendChild(newdiv);
    newdiv.innerHTML = copytext;
    selection.selectAllChildren(newdiv);
    window.setTimeout(function() {
        body_element.removeChild(newdiv);
    },0);
}

console.log(addLink);
document.oncopy = addLink;

</script>
        <!-- Google Analytics -->
        <!-- <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X');ga('send','pageview');
        </script>-->    

    </body>
</html>    