<!DOCTYPE html>
<!--[if lt IE 7]> <html class="no-js lt-ie9 lt-ie8 lt-ie7" lang="fr-FR" xmlns:og="http://ogp.me/ns#" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" itemscope itemtype="http://schema.org/WebPage"> <![endif]-->
<!--[if IE 7]> <html class="no-js lt-ie9 lt-ie8" xmlns:og="http://ogp.me/ns#" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" itemscope itemtype="http://schema.org/WebPage"> <![endif]-->
<!--[if IE 8]> <html class="no-js lt-ie9" xmlns:og="http://ogp.me/ns#" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" itemscope itemtype="http://schema.org/WebPage"> <![endif]-->
<!--[if gt IE 8]><!--> 
<html class="no-js" xmlns:og="http://ogp.me/ns#" prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb#" itemscope itemtype="http://schema.org/WebPage"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
        <title><?php echo get_bloginfo('name') ?></title>
        <meta name="description" content="">
        <meta name="viewport" content="initial-scale=1, user-scalable=no, maximum-scale=1, width=device-width">
        <!--====================FAVICON=======================-->
        <link rel="apple-touch-icon" sizes="57x57" href="favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="favicon/favicon-16x16.png">
        <link rel="manifest" href="favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">
        <!--====================FAVICON=======================-->
        <meta name="robots" content="noodp,noydir"/>
        <link rel="publisher" href=""/>
        <meta itemprop="name" content="">
        <meta itemprop="url" content="">
        <meta itemprop="image" content="">
        <meta itemprop="author" content="">
        <meta property='og:locale' content="fr"/>
        <meta property='og:type' content=""/>
        <meta property='og:title' content=""/>
        <meta property='og:url' content=""/>
        <meta property='og:site_name' content=""/>
        <meta property='article:publisher' content=""/>
        <meta property='fb:app_id' content=""/>
        <meta property='og:image' content=""/>
        <meta name="twitter:card" content=""/>
        <meta name="twitter:site" content=""/>
        <meta name="twitter:domain" content=""/>
        <meta name="twitter:creator" content=""/>
        <meta name="twitter:image:src" content=""/>
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/plugins.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/main.css">
        <link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/responsive.css">
        <!--[if lt IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="http://ie7-js.googlecode.com/svn/version/2.1(beta4)/IE9.js"></script>
            <noscript><link rel="stylesheet" href="<?php echo get_template_directory_uri() ?>/assets/css/stupid-ie.css" /></noscript>
        <![endif]-->
         <script type="text/javascript">
            var append_link =null;
        </script>
    </head>
    <body>
        <header>
            <div class="container clearfix">
               <ul class="lang">
                    <li class="active">
                        <a href="#">
                            fa
                        </a>
                    </li>
                    <li>
                        <a href="#">
                            en
                        </a>
                    </li>
                </ul>  <!--lang-->
                <a href="/" class="logo">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.png" />
                </a> <!--logo-->
                <nav>
                    <ul class="menu">
                        <li>
                            <a href="/">
                                صفحه اصلی
                            </a>
                        </li>
                        <li class="drop">
                            <a href="#">
                                مقالات
                            </a>
                            <ul>
                                <li>
                                    <a href="/article">
                                        مقالات
                                    </a>
                                </li>
                                <li>
                                    <a href="/critic">
                                        بررسی آثار
                                    </a>
                                </li>
                            </ul> 
                        </li> <!--drop-->
                        <li>
                            <a href="/events">
                                نمایشگاه ها
                            </a>
                        </li>
                        <li>
                            <a href="/artists">
                                هنرمندان
                            </a>
                        </li>
                        <li>
                            <a href="/send">
                                ارسال اثر
                            </a>
                        </li>
                        <li>
                            <a href="/contact">
                                تماس با ما
                            </a>
                        </li>
                    </ul> <!--menu-->
                    <a herf="#" class="search_btn">
                        <i class="icon search_icon"></i>
                    </a> 

                    
                </nav>
            </div> <!--container-->
        </header>
        <div class="toolbar_on_scroll">
            <div class="container">
                <a href="/" class="logo">
                    <img src="<?php echo get_template_directory_uri() ?>/assets/img/logo.png" />
                </a> <!--logo-->
                <nav>
                    <ul class="menu">
                        <li>
                            <a href="/">
                                صفحه اصلی
                            </a>
                        </li>
                        <li class="drop">
                            <a href="#">
                                مقالات
                            </a>
                            <ul>
                                <li>
                                    <a href="/article">
                                        مقالات
                                    </a>
                                </li>
                                <li>
                                    <a href="/critic">
                                        بررسی آثار
                                    </a>
                                </li>
                            </ul> 
                        </li> <!--drop-->
                        <li>
                            <a href="/events">
                                نمایشگاه ها
                            </a>
                        </li>
                        <li>
                            <a href="/artists">
                                هنرمندان
                            </a>
                        </li>
                        <li>
                            <a href="/submit-works">
                                ارسال اثر
                            </a>
                        </li>
                        <li>
                            <a href="/contact">
                                تماس با ما
                            </a>
                        </li>
                    </ul> <!--menu-->
                    <a herf="#" class="search_btn">
                        <i class="icon search_icon"></i>
                    </a>

                </nav>
            </div> <!--container-->
        </div> <!--toolbar_on_scroll-->

        <div class="search_layout">
            <a href="#" class="close_big">
                <i class="icon close_big_icon"></i>
            </a>
            <form id="search_frm" action="<?php echo get_site_url() ?>" method="GET" >
                <input type="submit" class="search" id="btn_search" value="" />
                <i class="icon search_big_icon"></i>
                <input id="search_place"  name="s" id="s" type="text" placeholder="" />
                
            </form>
        </div> <!--/search_layout-->