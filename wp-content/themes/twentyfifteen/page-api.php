<?php
	
	header('Content-Type: application/json');


	
	$offset = $_GET["offset"];
	$post_type = $_GET["type"];
	$args = null;



	if($post_type == "articles" || $post_type == "critics" ){
		$cat = $_GET["category"];
		$args = array(
			"posts_per_page"=>9,
			'post_type' => $post_type, 
			"offset"=>$offset,
			'tax_query' => array(
	            array(
	                'taxonomy' => 'type',
	                'field' => 'term_id',
	                'terms' => $cat,
	            ),
	        ),
			
		);
	}
	else{
		$args = array(
			"posts_per_page"=>9,
			'post_type' => $post_type, 
			"offset"=>$offset,	
		);
	}

	$psts = get_posts($args);
	
$result = [];
foreach ($psts as $key => $post) {
	$temp = $post;
	$temp->image = wp_get_attachment_url( get_post_thumbnail_id($temp->ID) );
	$temp->f_date = jdate('j, F',strtotime($temp->post_date));
	
	array_push($result, $temp);
}

echo json_encode($result);

?>