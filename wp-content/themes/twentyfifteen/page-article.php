<?php get_header(); 


$taxonom = get_term_by( 'name','article','type' );
$taxonom_id = $taxonom->term_id;


?>


<div class="main">
    <div class="container">
        <section class="section">
            <div class="row" id="box_container"> 
                <?php  $args = array(    'posts_per_page' => -1,  
                                         'post_type' => 'articles',
                                         'tax_query' =>array(
                                          array(
                                          'taxonomy'=>'type',
                                          'field' => 'id',
                                          'terms' => $taxonom_id

                                          ) )
                                          //'category'=>$taxonom_id
                                          ); 
                                          

                                
                                $articles = get_posts( $args );  
                                foreach ( $articles as $key=>$article ) :?>    
                                    <?php  if($key>=9) break; ?>

                                

                                <?php

                                  $fields = get_fields($article);
                                  $title_main = $article->post_title; 
                                  $content_main = $article->post_content;  
                                  $first_image = $fields['first_image'];
                                  //print_r($first_image)
                                  $time = get_the_date();
                                  //print_r($fields);


                                  echo '


                                  <div class="col-md-4 col-sm-12">
                                            <a href="' . get_permalink($article)  . '" class="box little_box">
                                            <div class="image_container">
                                                <div class="img_holder">
                                                    <img src="'.$first_image.'" />
                                                </div> <!--img_holder-->
                                                </div>
                                                <div class="brief_info">
                                                    <h2>'. $title_main .'</h2>
    
                                                    <time>'.$time.'</time>
                                                </div> <!--brief_info-->
                                            </a>
                                        </div>
                                    

                                  ';

                                  ?>


             <?php endforeach;?>
             </div> <!--row-->
             <?php 
                  
                  if (sizeof($articles) > 9):?>
                    <div class="more">
                          <a data-type="articles" data-catid="9" class="btn more_btn">
                              بیشتر
                          </a>
                     </div> <!--more-->
              <?php endif; ?>
        </section> <!--section-->
    </div> <!--container-->
</div> <!--main-->

<?php get_footer(); ?>