<?php get_header(); 

$page_home = get_page_by_title("home" );
$page_home_fileds = get_fields($page_home->ID);


?>
<div class="main">
            <div class="container">
                <section class="section">
                    <div class="row" id="box_container">
                      
                        


                        <?php  
                                
                                $featureA = get_field("field_563a1a2f571bc",$page_home->ID);
                                //print_r($slogan_small);
                                foreach ( $featureA as $article ) : ?>    
                                
                                

                                <?php

                                  $fields = get_fields($article);
                                  $title_main = $article->post_title;  
                                  $first_image = $fields['first_image'];
                                  $time = get_the_date();


                                  echo '<div class="col-md-4 col-sm-12">
                                      <a href="' . get_permalink($article)  . '" class="box little_box">
                                          <div class="image_container">
                                            <div class="img_holder">
                                                <img src="'. $first_image .'" />
                                            </div> <!--img_holder-->
                                            </div>
                                          <div class="brief_info">
                                              <h3>مقاله</h3>
                                              <h2>'. $title_main .'</h2>
                                              <time>'. $time .'</time>
                                          </div> <!--brief_info-->
                                      </a>
                                  </div>';

                                  ?>


                        <?php endforeach;?>





                        <?php  

                                 $featureB = get_field("field_563a69848ae66",$page_home->ID);
                                foreach ( $featureB as $artist ) : ?>    
                                
                                

                                <?php

                                  $fields = get_fields($artist);
                                  $title_main = $artist->post_title; 
                                  //print_r($fields);
                                  $first_image = $fields['first_image'];
                                  $time = get_the_date();


                                  echo '<div class="col-md-4 col-sm-12">
                                      <a href="' . get_permalink($artist)  . '" class="box little_box">
                                        <div class="image_container">
                                            <div class="img_holder">
                                                <img src="'. $first_image .'" />
                                            </div> <!--img_holder-->
                                            </div>
                                          <div class="brief_info">
                                              <h3>هنرمندان</h3>
                                              <h2>'. $title_main .'</h2>
                                              <time>'. $time .'</time>
                                          </div> <!--brief_info-->
                                      </a>
                                  </div>';

                                  ?>


                        <?php endforeach;?>


                        <?php  

                                 $featureC = get_field("field_563a69c367d9e",$page_home->ID);

                                foreach ( $featureC as $event ) : ?>    
                                
                                

                                <?php

                                  $fields = get_fields($event);
                                  $title_main = $event->post_title; 
                                  //print_r($fields);
                                  $first_image = $fields['first_image'];
                                  $time = get_the_date();


                                  echo '<div class="col-md-4 col-sm-12">
                                      <a href="' . get_permalink($event)  . '" class="box little_box">
                                        <div class="image_container">
                                            <div class="img_holder">
                                                <img src="'. $first_image .'" />
                                            </div> <!--img_holder-->
                                            </div>
                                          <div class="brief_info">
                                              <h3>نمایشگاه ها</h3>
                                              <h2>'. $title_main .'</h2>
                                              <time>'. $time .'</time>
                                          </div> <!--brief_info-->
                                      </a>
                                  </div>';

                                  ?>


                        <?php endforeach;?>








                    </div> <!--row-->
                <section> <!--section-->
            </div> <!--container-->
        </div> <!--main-->
<?php get_footer(); ?>