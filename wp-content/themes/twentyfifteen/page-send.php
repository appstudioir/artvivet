<?php get_header(); ?>
<?php 
         $res = false;
        if(isset($_POST["artist_name"])){
                $pst  = array(
                    'post_title' => $_POST["artist_name"]." ".$_POST["email"],
                    'post_content'=>$_POST["description"],
                    
                    'post_type' =>"sent_works",
                    
                );
                $post_id = wp_insert_post($pst);
                update_field('field_5662d5e0fc056',$_POST["artist_name"],$post_id);
                update_field('field_5662d5edfc057',$_POST["email"],$post_id);
               if (!function_exists('wp_handle_upload'))
                { 
                     require_once( ABSPATH . '/wp-admin/includes/file.php' );
                }
        
                if($_FILES["file"])
                    
               {
                        
                       
                       $uploadedfile = $_FILES['file'];
                     
                       $upload_overrides = array( 'test_form' => FALSE );
                       $movefile = wp_handle_upload( $uploadedfile, $upload_overrides );
                        
                      if ( $movefile ) {
                            $wp_filetype = $movefile['type'];
                            $filename = $movefile['file'];
                            $wp_upload_dir = wp_upload_dir();
                            $attachment = array(
                                'guid' => $wp_upload_dir['url'] . '/' . basename( $filename ),
                                'post_mime_type' => $wp_filetype,
                                'post_title' => preg_replace('/\.[^.]+$/', '', basename($filename)),
                                'post_content' => '',
                                'post_status' => 'inherit'
                            );
                            $attach_id = wp_insert_attachment( $attachment, $filename);
                        
                           $attach_id = wp_insert_attachment( $attachment, $filename);
                            update_field('field_5662d60057ad8',$attach_id,$post_id);
                            $res = true;
                       }
                       
                      
               
                          
               
                  
                      
                }
            }
        
    
    
?>
<script type="text/javascript">
    var res = <?php echo $res ?>;
    if(res){
        alert("ok");
    }
</script>
<div class="main contact">
    <div class="container">
        <section class="section">
            <div class="row owner clearfix">
                <div class="col-md-3 col-sm-12">
                    <div class="img_holder">
                        <img src="assets/img/kiana.png" />
                    </div> <!--img_holder-->
                </div>
                <div class="col-md-9 col-sm-12">
                    <h4>کیانا نخجوانی</h4>
                    <p>
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیستری را برای طراحان رایانه ای و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
                    </p>
                </div>
            </div> <!--owner-->
            <div class="row owner clearfix">
                <div class="col-md-9 col-sm-12">
                    <h4>لیلا محسن پور</h4>
                    <p>
                        لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیستری را برای طراحان رایانه ای و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
                    </p>
                </div>
                <div class="col-md-3 col-sm-12">
                    <div class="img_holder">
                        <img src="assets/img/leila.png" />
                    </div> <!--img_holder-->
                </div>
            </div> <!--owner-->
            <h3>لورم ایپسوم متن ساختگی</h3>
            <form class="filling_form" method="POST" action="/contact" enctype="multipart/form-data">
                <fieldset class="row">
                    <div class="required name col-md-6 col-sm-12">
                        <input type="text" placeholder="نام" name="user_name" />
                        <i class="icon star_icon"></i>
                    </div>
                    <div class="required email col-md-6 col-sm-12">
                        <input type="email" placeholder="ایمیل" name="email" />
                        <i class="icon star_icon"></i>
                    </div>
                </fieldset> <!--row-->

                
                <fieldset class="required message">
                    <textarea type="text" placeholder="توضیحات" name="description"></textarea>
                    <i class="icon star_icon"></i>
                </fieldset>
                <fieldset class="uploader">
                    <div class="box">
                        <input type="file" name="file-1[]" id="file-1" class="inputfile inputfile-1" data-multiple-caption="{count} files selected" multiple />
                        <label for="file-1"><svg xmlns="http://www.w3.org/2000/svg" width="20" height="17" viewBox="0 0 20 17"><path d="M10 0l-5.2 4.9h3.3v5.1h3.8v-5.1h3.3l-5.2-4.9zm9.3 11.5l-3.2-2.1h-2l3.4 2.6h-3.5c-.1 0-.2.1-.2.1l-.8 2.3h-6l-.8-2.2c-.1-.1-.1-.2-.2-.2h-3.6l3.4-2.6h-2l-3.2 2.1c-.4.3-.7 1-.6 1.5l.6 3.1c.1.5.7.9 1.2.9h16.3c.6 0 1.1-.4 1.3-.9l.6-3.1c.1-.5-.2-1.2-.7-1.5z"/></svg> <span>انتخاب فایل&hellip;</span></label>
                    </div>
                    <p>
                        متن در رابطه با حجم تصاویر
                    </p>
                    <input class="btn" id="submit" type="submit" value="ارسال" />
                </fieldset> <!--uploader-->
            </form> <!--filling_form-->
        </section> <!--section-->
    </div> <!--container-->
</div> <!--main-->
<?php get_footer(); ?>