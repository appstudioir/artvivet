<?php
/**
 * The template for displaying search results pages.
 *
 * @package WordPress
 * @subpackage Twenty_Fifteen
 * @since Twenty Fifteen 1.0
 */

get_header(); ?>
<div class="main">
            <div class="container">
                 <section class="section">
                  <div class="row" id="box_container">

                          <?php  foreach ($posts as $key => $article) :

                          	 $fields = get_fields($article);
                          	  $title_main = $article->post_title; 
                          	  $content_main = $article->post_content;  
                          	  $first_image = $fields['first_image'];
                          	  //print_r($first_image)
                          	  $time = get_the_date();
                          ?>

                                    
                                


            	                     <div class="col-md-4 col-sm-12">
                                     <a href="<?php echo get_permalink($article->ID)  ?>" class="box little_box">
                                          <div class="img_holder">
                                              <img src="<?php echo  $first_image ?>" />
                                          </div>
                                          <div class="brief_info">
                                             
                                              <h2><?php echo $title_main ?></h2>
                                              <time><?php echo $time  ?></time>
                                          </div> 
                                      </a>
                                  </div>

                                  <?php endforeach ?>

                  </div>
                                


            

              </section>
            </div>
          </div>

<?php get_footer(); ?>
