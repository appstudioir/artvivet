<?php get_header(); ?>


<?php 

     if ($post->post_type=='articles'): ?>
    
<?php 

     $fields = get_fields($post->ID);
     $title_main = $post->post_title;
     $post_images = $fields['first_image'];
     $post_author = $fields['author'];
     $post_date = $post->post_date;
     $post_content = $post->post_content;



?>
<div class="main inner">
    <div class="container">
        <section class="section">
            <div class="img_holder feature_image">
                <img src="<?php echo $post_images; ?>" />
            </div> <!--feature_image-->
            <div class="container mini_container">
                <h1><?php echo $title_main; ?> </h1>
                <div class="artist_info">
                    <h5><?php echo $post_author ; ?></h5>
                    <span></span>
                    <time><?php echo get_the_date() ; ?></time>
                </div> <!--artist_info-->
                <div class="post_content_holder">
                        <?php echo $post_content; ?>
                         <!-- <p>
                            لورم ایپسوم متن ساختگی با تولید سادگی نامفهوم از صنعت چاپ و با استفاده از طراحان گرافیک است. چاپگرها و متون بلکه روزنامه و مجله در ستون و سطرآنچنان که لازم است و برای شرایط فعلی تکنولوژی مورد نیاز و کاربردهای متنوع با هدف بهبود ابزارهای کاربردی می باشد. کتابهای زیادی در شصت و سه درصد گذشته، حال و آینده شناخت فراوان جامعه و متخصصان را می طلبد تا با نرم افزارها شناخت بیستری را برای طراحان رایانه ای و فرهنگ پیشرو در زبان فارسی ایجاد کرد.
                            در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
                        </p>
                        <blockquote>
                            در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل.
                        </blockquote>
                        <h4>لورم ایپسوم متن ساختگی</h4>
                        <p>
                            در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
                        </p>
                        <div class="img_holder">
                            <img src="assets/media/cactous.png" />
                        </div> feature_image
                         <p>
                            در این صورت می توان امید داشت که تمام و دشواری موجود در ارائه راهکارها و شرایط سخت تایپ به پایان رسد وزمان مورد نیاز شامل حروفچینی دستاوردهای اصلی و جوابگوی سوالات پیوسته اهل دنیای موجود طراحی اساسا مورد استفاده قرار گیرد.
                        </p>
                        <h4>لورم ایپسوم متن ساختگی</h4>  -->

                </div>
				<div class="share">
				<h4>به اشتراک گذاری</h4>
				<ul>
				<li>
				<a href="#">
				<i class="icon facebook_icon"></i>
				</a>
				</li>
				<li>
				<a href="#">
				<i class="icon twitter_icon"></i>
				</a>
				</li>
				<li>
				<a href="#">
				<i class="icon google_icon"></i>
				</a>
				</li>
				<li class="dont_show">
				<a href="#">
				<i class="icon viber_icon"></i>
				</a>
				</li>
				<li class="dont_show">
				<a href="#">
				<i class="icon line_icon"></i>
				</a>
				</li>
				<li class="dont_show">
				<a href="#">
				<i class="icon whatsapp_icon"></i>
				</a>
				</li>
				</ul>
				</div>
                <h3>نظرات</h3>

                <div class="comments">
                     <?php comments_template(); ?>
                    
                </div> <!--comments-->
                
            </div> <!--mini_container-->
        </section> <!--section-->
    </div> <!--container-->
</div> <!--main-->


<?php endif ?>

<?php if ($post->post_type=='artists'):

    $artist_fields = get_fields($post->ID);
     $title_main = $post->post_title;
     $post_images = $artist_fields['first_image'];
     $post_author = $artist_fields['author'];
     $post_date = $post->post_date;
     $post_content = $post->post_content;
     $artist_gallery = $artist_fields['gallery']


?>


<div class="main inner">
    <div class="container">
        <section class="section">
            <div class="img_holder feature_image artist_image">
                <img src="<?php echo $post_images; ?>" />
            </div> <!--feature_image-->
            <div class="container mini_container">
                <h1><?php echo $title_main; ?> </h1>
                <div class="artist_info">
                   
                    <time><?php echo get_the_date() ; ?></time>
                </div> <!--artist_info-->

                <div class="post_content_holder">
                        <?php echo $post_content; ?>

                </div>
                <h4 id="gallery">گالری تصاویر</h4>
                
                <ul class="gallery clearfix">

                    <?php foreach ($artist_gallery as $key => $image):?>

                            <li>
                                <a   href="<?php echo $image['image']  ?>" rel="fancybox-thumb" class="img_holder fancybox-thumb" >
                                    <img src="<?php echo $image['image']  ?>"/>
                                </a>
                            </li>

                    <?php endforeach; ?>

                </ul> <!--gallery-->

                
                
                
            </div> <!--mini_container-->
        </section> <!--section-->
    </div> <!--container-->
</div> <!--main-->


<?php endif ?>

<?php if ($post->post_type=='events'):

    $event_fields = get_fields($post->ID);
     $title_main = $post->post_title;
     $post_images = $event_fields['first_image'];
     $post_author = $event_fields['artist'];
     $post_date = $post->post_date;
     $post_content = $post->post_content;
     $place = $event_fields['place'];
     $address = $event_fields['address'];
     $time = $event_fields['time'];
     $openning = $event_fields['openning'];
     $hours = $event_fields['hours'];


?>


<div class="main inner">
    <div class="container">
        <section class="section">
            <div class="img_holder feature_image">
                <img src="<?php echo $post_images; ?>" />
            </div> <!--feature_image-->
            <div class="container mini_container">
                <h1><?php echo $title_main; ?> </h1>
                <div class="artist_info">
                    <h5><?php echo $post_author ; ?></h5>
                    <span></span>
                    <time><?php echo get_the_date() ; ?></time>
                </div> <!--artist_info-->
                <div class="info">
                    <ul>
                        <li class="clearfix">
                            <span>مکان:</span>
                            <p>
                                <?php echo $place; ?>
                            </p>
                        </li>
                        <li class="clearfix">
                            <span>زمان:</span>
                            <p>
                                <?php echo $time; ?>
                            </p>
                        </li>
                        <li class="clearfix">
                            <span>آدرس:</span>
                            <p>
                                <?php echo $address; ?>
                            </p>
                        </li>
                        <li class="clearfix">
                            <span>افتتاحیه:</span>
                            <p>
                               <?php echo $openning; ?>
                            </p>
                        </li>
                        <li class="clearfix">
                            <span>ساعات:</span>
                            <p>
                               <?php echo $hours; ?>
                            </p>
                        </li>
                    </ul>
                </div> <!--info-->
                <div class="post_content_holder">
                        <?php echo $post_content; ?>

                </div>
                
                <h3>نظرات</h3>

                <div class="comments">
                     <?php comments_template(); ?>
                    
                </div> <!--comments-->
                
            </div> <!--mini_container-->
        </section> <!--section-->
    </div> <!--container-->
</div> <!--main-->


<?php endif ?>
<?php get_footer(); ?>